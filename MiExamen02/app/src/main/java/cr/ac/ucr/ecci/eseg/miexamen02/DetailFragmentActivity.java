package cr.ac.ucr.ecci.eseg.miexamen02;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class DetailFragmentActivity  extends AppCompatActivity {
    public DetailFragmentActivity() {
        super(R.layout.fragment_container);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Restaurante item = getIntent().getExtras().getParcelable("RESTAURANTE");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, new listFragmentView(item), null)
                    .commit();
        }
    }
}