package cr.ac.ucr.ecci.eseg.miexamen02;

import java.util.List;

public interface IServiceDataSource {
    List<Restaurante> obtainItems ();
}
