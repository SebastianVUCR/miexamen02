package cr.ac.ucr.ecci.eseg.miexamen02;

import java.util.List;


/**
 *  Clase entre el controlador (RestauranteInteractor)
 *  Clase intermedia entre la vista y la capa de datos
 */
public class Controller implements IController{

    MainActivity mainActivity;
    ServiceDataSource serviceDataSource;
    private static final int ESTIMATED_LENGTH = 12;

    public Controller (MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.serviceDataSource = new ServiceDataSource(mainActivity, ESTIMATED_LENGTH);
    }

    @Override
    public void requestItems(String url) {
        this.serviceDataSource.requesItems(url, this);
    }

    @Override
    public void updateMainActivityView() {

        mainActivity.updateList(this.getObtainItems());
    }

    public List<Restaurante> getObtainItems() {
        return serviceDataSource.obtainItems();
    }
}
