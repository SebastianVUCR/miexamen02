package cr.ac.ucr.ecci.eseg.miexamen02;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LazyAdapter extends ArrayAdapter<Restaurante> {
    private final Activity context;
    private final List<Restaurante>  items;

    //Constructor de la clase, obtiene los datos que se van a mostrar
    public LazyAdapter(Activity context, List<Restaurante> items) {
        super(context, R.layout.activity_main, items);
        this.context = context;
        this.items = items;
        int depurar = 1;
    }
    //Pone los datos en la vista y muestra la vista
    public View getView(int position, View view, ViewGroup parent) {
        ArrayList<Restaurante> items = new ArrayList<>(this.items);
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.activity_main, null, true);
        TextView nombre = (TextView) rowView.findViewById(R.id.name);
        nombre.setText(items.get(position).getNombre());
        TextView descripcion = (TextView) rowView.findViewById(R.id.rating);
        descripcion.setText(items.get(position).getRating() + " / 5"); return rowView; }
}
