package cr.ac.ucr.ecci.eseg.miexamen02;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.List;


public class ServiceDataSource implements IServiceDataSource {

    private StringBuilder  mDataText;
    private String data;
    private ProgressDialog mDialog;
    private Context context;
    private int estimatedLenght = 0;
    private DocumentParser parser = new DocumentParser();
    private Controller controller;


    public ServiceDataSource(Context context) {
        this.context = context;
        this.controller = controller;
    }

    public ServiceDataSource(Context context, int estimatedLines) {
        this.context = context;
        this.estimatedLenght = estimatedLines;
    }

    public void requesItems(String url, Controller controller) {
        this.controller = controller;
        mDialog = new ProgressDialog(context);
        mDialog.setMessage("Obtiene datos...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(estimatedLenght);
        mDialog.show();
        new TaskServicioREST().execute(url);
    }

    private class TaskServicioREST extends AsyncTask<String, Float, String> {

        private int currentLine = 0;

        @Override protected void onPreExecute() {
            // antes de ejecutar
            mDialog.setProgress(0);
        }
        @Override protected String doInBackground(String... urls) {
            try {
                InputStream mInputStream = (InputStream) new URL(urls[0]).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder();

                String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                    currentLine += 1;
                    Thread.sleep(50);
                    publishProgress(currentLine/12f);
                }
                currentLine = estimatedLenght;
                return strBuilder.toString();
            } catch (Exception e) {
                Log.e("ERROR:"+"ServiceDataSource", e.getMessage());
            }
            return null;
        }
        protected void onProgressUpdate (Float... values) {
            // actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }
        protected void onPostExecute(String list) {
            data = list;
            mDialog.dismiss();
            controller.updateMainActivityView();
        }
    }

    @Override
    public List<Restaurante> obtainItems() {
        List<Restaurante> items = parser.parseRestaurante(data);
        return items;
    }
}//ServiceDataSource(CLASE)
