package cr.ac.ucr.ecci.eseg.miexamen02;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class DocumentParser {
    public DocumentParser() {}

    public List<Restaurante> parseRestaurante(String document){
        List<Restaurante> restaurantes = new ArrayList<>();
            JsonReader jsonReader = null;
            /** inicializando jsonreader, try catch necesario*/
            jsonReader = new JsonReader(new StringReader(document));
            try {
                jsonReader.beginObject();// Quitando el encabezado del archivo json
                jsonReader.nextName();// Quitando el encabezado del archivo json
                jsonReader.beginArray();//empezando array
                jsonReader.beginObject();//empezando el primer objeto del array
                Restaurante restaurante = new Restaurante();
                while (jsonReader.hasNext() && jsonReader.peek().toString()!="END_DOCUMENT") {
                    String name = jsonReader.nextName();
                    if (name.equals("delivery")) {
                        Boolean bool = jsonReader.nextBoolean();
                        restaurante.setDelivery(bool);

                    } else if (name.equals("takeout")) {
                        Boolean bool = jsonReader.nextBoolean();
                        restaurante.setTakeout(bool);
                        //Log.v("NAME_JSON",text);

                    } else if (name.equals("id")) {
                        String text = jsonReader.nextString();
                        restaurante.setIdentificacion(text);
                        //Log.v("NAME_JSON",text);

                    } else if (name.equals("nombre")) {
                        String text = jsonReader.nextString();
                        restaurante.setNombre(text);
                        //Log.v("NAME_JSON",text);

                    } else if (name.equals("rating")) {
                        String text = jsonReader.nextString();
                        restaurante.setRating(text);
                        //Log.v("NAME_JSON",text);

                    } else if (name.equals("costo")) {
                        String text = jsonReader.nextString();
                        restaurante.setCosto(text);
                        //Log.v("NAME_JSON",text);

                    } else if (name.equals("telefono")) {
                        String text = jsonReader.nextString();
                        restaurante.setTelefono(text);
                        restaurantes.add(restaurante);
                        restaurante = new Restaurante();
                        jsonReader.endObject();
                        //Log.v("NAME_JSON",text);
                        if(jsonReader.peek().toString().equals("END_ARRAY")) {
                            //ultimo elemento del array y del documento
                            jsonReader.endArray();
                            jsonReader.endObject();
                        }else {
                            jsonReader.beginObject();//fin de objeto porque es el ultimo
                            /**
                             * TO DO: CREAR EL OBJETO Y AGREGARLO A LA LISTA
                             * */
                        }//ultimo else donde se inicia otro objeto si no se ha terminado
                        } else {
                            jsonReader.skipValue();//CUANDO ES INUTIL
                    }
                }
            } catch (Exception e) {
                Log.e("ERROR:ENCABEZADO","error quitando el encabezado del archivo json," +
                        " verifique que el archivo tiene el formato requerido. Error atrapado ->"+
                        e.getMessage());
            }
            /** Aqui deberia haber una lista de elementos json*/
            //Log.d("PRUEBA_PARSE",jsonReader.toString());
        return restaurantes;
    }
}
