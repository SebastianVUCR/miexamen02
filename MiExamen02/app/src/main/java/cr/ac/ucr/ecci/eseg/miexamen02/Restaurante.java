package cr.ac.ucr.ecci.eseg.miexamen02;

import android.os.Parcel;
import android.os.Parcelable;

public class Restaurante implements Parcelable {
    private String identificacion;
    private String nombre;
    private String rating;
    private String costo;
    private Boolean delivery;
    private Boolean takeout;
    private String telefono;

    public Restaurante (){}

    public Restaurante (String identificacion, String nombre, String rating, String costo, Boolean delivery, Boolean takeout, String telefono) {
        this.identificacion=identificacion;
        this.nombre = nombre;
        this.rating = rating;
        this.costo = costo;
        this.delivery = delivery;
        this.takeout = takeout;
        this.telefono = telefono;
    }

    protected Restaurante(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
        rating = in.readString();
        costo = in.readString();
        byte tmpDelivery = in.readByte();
        delivery = tmpDelivery == 0 ? null : tmpDelivery == 1;
        byte tmpTakeout = in.readByte();
        takeout = tmpTakeout == 0 ? null : tmpTakeout == 1;
        telefono = in.readString();
    }

    public static final Creator<Restaurante> CREATOR = new Creator<Restaurante>() {
        @Override
        public Restaurante createFromParcel(Parcel in) {
            return new Restaurante(in);
        }

        @Override
        public Restaurante[] newArray(int size) {
            return new Restaurante[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeString(rating);
        dest.writeString(costo);
        dest.writeByte((byte) (delivery == null ? 0 : delivery ? 1 : 2));
        dest.writeByte((byte) (takeout == null ? 0 : takeout ? 1 : 2));
        dest.writeString(telefono);
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public Boolean getDelivery() {
        return delivery;
    }

    public void setDelivery(Boolean delivery) {
        this.delivery = delivery;
    }

    public Boolean getTakeout() {
        return takeout;
    }

    public void setTakeout(Boolean takeout) {
        this.takeout = takeout;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}