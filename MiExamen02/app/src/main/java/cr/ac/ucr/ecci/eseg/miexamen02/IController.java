package cr.ac.ucr.ecci.eseg.miexamen02;

public interface IController {
    public void requestItems(String url);
    public void updateMainActivityView();
}
