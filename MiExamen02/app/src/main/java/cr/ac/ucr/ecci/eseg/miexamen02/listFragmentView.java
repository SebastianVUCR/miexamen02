package cr.ac.ucr.ecci.eseg.miexamen02;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link listFragmentView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class listFragmentView extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private Restaurante mParam;

    public listFragmentView(Restaurante item) {
        // Required empty public constructor
        super(R.layout.fragment_list_view);
        this.mParam = item;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param Parameter 1.
     * @return A new instance of fragment listFragmentView.
     */
    // TODO: Rename and change types and number of parameters
    public static listFragmentView newInstance(Restaurante param) {
        listFragmentView fragment = new listFragmentView(param);
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getParcelable(ARG_PARAM1);
        }
        //Toast.makeText(getActivity(), mParam.getNombre(), Toast.LENGTH_LONG).show();
        //TextView nombre = (TextView) getActivity().findViewById(R.id.name);
        //nombre.setText(mParam.getNombre());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inf = inflater.inflate(R.layout.fragment_list_view, container, false);
        TextView name = (TextView) inf.findViewById(R.id.textName);
        name.setText(mParam.getNombre());

        TextView id = (TextView) inf.findViewById(R.id.textID);
        id.setText(mParam.getIdentificacion());

        TextView rating = (TextView) inf.findViewById(R.id.textDetails);
        rating.setText(mParam.getRating().toString());

        TextView costo = (TextView) inf.findViewById(R.id.labelCosto);
        costo.setText(mParam.getCosto());

        TextView deliv = (TextView) inf.findViewById(R.id.labelDelibery);
        deliv.setText(mParam.getDelivery().toString());

        TextView takout = (TextView) inf.findViewById(R.id.labelTakeout);
        takout.setText(mParam.getTakeout().toString());

        TextView tel = (TextView) inf.findViewById(R.id.labelTelefono);
        tel.setText(mParam.getTelefono().toString());
        return inf;
    }
}