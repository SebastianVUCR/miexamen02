package cr.ac.ucr.ecci.eseg.miexamen02;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IMainActivity {

    private static final String SOURCE_DATA_URL = "https://bitbucket.org/lyonv/ci0161_ii2020_examenii/raw/ff2e92bc4e5c9b16a5977c8e3eed8395d4da792e/Restaurantes7.json";
    private Controller controller;
    private float progress;
    List<Restaurante> lista;
    ListView list;

    /**
     * Nota general: Se le cambio el nombre a RestauranteInteractor por Controller
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        controller = new Controller(this);
        controller.requestItems(SOURCE_DATA_URL);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void updateList(List<Restaurante> lista) {
        //Restaurantes.setRestaurantes(lista);
        setContentView(R.layout.list_container);
        Toast.makeText(this,"llamado a actualizar vista con: "+lista.toString(),Toast.LENGTH_SHORT);

         LazyAdapter adapter = new LazyAdapter(this, lista);
         list = (ListView) findViewById(R.id.list);
         list.setAdapter(adapter);
         list.setOnItemClickListener(new AdapterView.OnItemClickListener()
         {
         //listeneres de las opciones del menu
         @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             Intent intent = new Intent(getApplicationContext(), DetailFragmentActivity.class);
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             intent.putExtra("RESTAURANTE", new ArrayList<Restaurante>(lista).get(position));
             getApplicationContext().startActivity(intent);
         }
         });//listeners de la lista
    }

    public void showDetail(Restaurante item) {
        Intent intent = new Intent(this, DetailFragmentActivity.class);
        intent.putExtra("RESTAURANTE", item);
        this.startActivity(intent);
    }

}