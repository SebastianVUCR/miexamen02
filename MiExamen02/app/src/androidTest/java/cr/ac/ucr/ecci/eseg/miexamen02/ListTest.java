package cr.ac.ucr.ecci.eseg.miexamen02;

import org.junit.Rule;
import org.junit.Test;


import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.runner.RunWith;

import android.content.Context;
import android.content.Intent;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Test;

import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertEquals;

import static androidx.test.espresso.Espresso.onView;
import static junit.framework.TestCase.assertNotNull;

import androidx.test.core.app.ActivityScenario;


@RunWith(AndroidJUnit4.class)
public class ListTest {
    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void mainViewTest() {
        ActivityScenario scenario = activityScenarioRule.getScenario();
        assertNotNull(onView(withId(R.layout.activity_main)));
    }
}
